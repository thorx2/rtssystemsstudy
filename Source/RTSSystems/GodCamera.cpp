// All rights reserved Innocent Sign Inn

#include "GodCamera.h"
#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"
#include "GodPlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Blueprint/WidgetLayoutLibrary.h"

AGodCamera::AGodCamera()
{
	PrimaryActorTick.bCanEverTick = true;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bDoCollisionTest = false;
	MainCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("MainCamera"));
	MainCamera->SetupAttachment(CameraBoom);
}

void AGodCamera::BeginPlay()
{
	Super::BeginPlay();
	
	if (auto PlayerController = Cast<AGodPlayerController>(GetController()))
	{
		GodPlayerController = PlayerController;
		auto LocalPlayer = PlayerController->GetLocalPlayer();

		if (auto* InputSystem = LocalPlayer->GetSubsystem<UEnhancedInputLocalPlayerSubsystem>())
		{
			InputSystem->AddMappingContext(InputMapping, 5000);
		}
		UE_LOG(LogTemp, Warning, TEXT("Things worked out"));
	}

	TargetLocation = GetActorLocation();
	// Default Zoom level on start -> Could be a variable
	TargetZoom = 3000;

	if (CameraBoom != nullptr)
	{
		const FRotator InitialRotation = CameraBoom->GetRelativeRotation();
		TargetRotation = FRotator(InitialRotation.Pitch + -50, InitialRotation.Yaw, 0.0f);
		UE_LOG(LogTemp, Warning, TEXT("Works!!"));
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Stupid C++"));
	}
}

void AGodCamera::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	UEnhancedInputComponent* Input = Cast<UEnhancedInputComponent>(PlayerInputComponent);
	Input->BindAction(PanAction, ETriggerEvent::Triggered, this, &AGodCamera::MoveActionCallbackFunc);
	Input->BindAction(RotateAction, ETriggerEvent::Started, this, &AGodCamera::RotateCamera);
	Input->BindAction(ZoomAction, ETriggerEvent::Triggered, this, &AGodCamera::ZoomCamera);
	Input->BindAction(LeftMouseDown, ETriggerEvent::Started, this, &AGodCamera::OnLeftMouseDownCallbackFunc);
	Input->BindAction(LeftMouseDown, ETriggerEvent::Completed, this, &AGodCamera::OnLeftMouseUpCallbackFunc);
}

void AGodCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (CameraBoom)
	{
		const FRotator InterpolatedRotation = UKismetMathLibrary::RInterpTo(CameraBoom->GetRelativeRotation(),
		                                                                    TargetRotation, DeltaTime, RotateSpeed);
		CameraBoom->SetRelativeRotation(InterpolatedRotation);

		const FVector InterpolatedLocation = UKismetMathLibrary::VInterpTo(
			GetActorLocation(), TargetLocation, DeltaTime,
			PanMovementSpeed);
		SetActorLocation(InterpolatedLocation);

		const float InterpolatedBoomLength = UKismetMathLibrary::FInterpTo(CameraBoom->TargetArmLength, TargetZoom,
		                                                                   DeltaTime, ZoomInSpeed);
		CameraBoom->TargetArmLength = InterpolatedBoomLength;

		EdgeScroll();
	}
	else
	{
		// UE_LOG(LogTemp, Error, TEXT("We have a problem!!"));
	}
}


void AGodCamera::MoveActionCallbackFunc(const FInputActionInstance& Instance)
{
	auto forwardInput = Instance.GetValue().Get<FVector2d>();
	TargetLocation = CameraBoom->GetForwardVector() * forwardInput.Y * PanMovementSpeed + TargetLocation;
	TargetLocation = CameraBoom->GetRightVector() * forwardInput.X * PanMovementSpeed + TargetLocation;
	GetTerrainPosition(TargetLocation);
}

void AGodCamera::EdgeScroll()
{
	FVector2D MousePosition = UWidgetLayoutLibrary::GetMousePositionOnViewport(GetWorld()) *
		UWidgetLayoutLibrary::GetViewportScale(GetWorld());
	const FVector2D ViewportSize = UWidgetLayoutLibrary::GetViewportSize(GetWorld());
	MousePosition.X /= ViewportSize.X;
	MousePosition.Y /= ViewportSize.Y;

	if (MousePosition.X > 0.95f && MousePosition.X < 1.f)
	{
		TargetLocation = CameraBoom->GetRightVector() * 1 * PanMovementSpeed + TargetLocation;
	}
	else if (MousePosition.X < 0.02f && MousePosition.X > 0.f)
	{
		TargetLocation = CameraBoom->GetRightVector() * -1 * PanMovementSpeed + TargetLocation;
	}


	if (MousePosition.Y > 0.95f && MousePosition.Y < 1.f)
	{
		TargetLocation = CameraBoom->GetForwardVector() * -1 * PanMovementSpeed + TargetLocation;
	}
	else if (MousePosition.Y < 0.02f && MousePosition.Y > 0.f)
	{
		TargetLocation = CameraBoom->GetForwardVector() * 1 * PanMovementSpeed + TargetLocation;
	}
	GetTerrainPosition(TargetLocation);
}

void AGodCamera::GetTerrainPosition(FVector& TerrainPosition) const
{
	FHitResult Hit;
	FCollisionQueryParams CollisionParams;
	FVector TraceOrigin = TerrainPosition;
	TraceOrigin.Z += 10000.f;
	FVector TraceEnd = TerrainPosition;
	TraceEnd.Z -= 10000.f;

	if (UWorld* World = GetWorld())
	{
		if (World->LineTraceSingleByChannel(Hit, TraceOrigin, TraceEnd, ECC_Visibility, CollisionParams))
		{
			TerrainPosition = Hit.ImpactPoint;
		}
	}
}

AActor* AGodCamera::GetSelectedObject()
{
	if (UWorld* World = GetWorld())
	{
		FVector WorldLocation, WorldDirection;
		GodPlayerController->DeprojectMousePositionToWorld(WorldLocation, WorldDirection);
		FVector End = WorldDirection * 100000.f + WorldLocation;
		FCollisionQueryParams CollisionQueryParams;
		CollisionQueryParams.AddIgnoredActor(this);
		FHitResult Hit;
		if (World->LineTraceSingleByChannel(Hit, WorldLocation, End, ECC_Visibility, CollisionQueryParams))
		{
			if (auto HitActor = Hit.GetActor())
			{
				return HitActor;
			}
		}
	}

	return nullptr;
}

void AGodCamera::RotateCamera(const FInputActionInstance& Instance)
{
	const auto Direction = Instance.GetValue().Get<float>();

	TargetRotation = UKismetMathLibrary::ComposeRotators(TargetRotation, FRotator(0, 45 * Direction, 0));
}

void AGodCamera::ZoomCamera(const FInputActionInstance& Instance)
{
	const auto Direction = Instance.GetValue().Get<float>();
	const float Delta = Direction * ZoomInSpeed;
	TargetZoom = FMath::Clamp(Delta + TargetZoom, MinZoomHeight, MaxZoomHeight);
}

void AGodCamera::OnLeftMouseDownCallbackFunc(const FInputActionInstance& Instance)
{
}

void AGodCamera::OnLeftMouseUpCallbackFunc(const FInputActionInstance& Instance)
{
	if (GodPlayerController)
	{
		GodPlayerController->Handle_Selection(GetSelectedObject());
	}
}
