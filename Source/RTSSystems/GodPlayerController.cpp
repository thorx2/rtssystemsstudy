// All rights reserved Innocent Sign Inn


#include "GodPlayerController.h"
#include "ISelectable.h"
#include "Net/UnrealNetwork.h"

AGodPlayerController::AGodPlayerController(const FObjectInitializer& ObjectInitializer)
{
}

void AGodPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(AGodPlayerController, Selected, COND_OwnerOnly);
}

void AGodPlayerController::Handle_Selection(AActor* ActorToSelect)
{
	if (auto Selectable = Cast<IISelectable>(ActorToSelect))
	{
		if (ActorToSelect && ActorSelected(ActorToSelect))
		{
			Server_DeSelect(ActorToSelect);
		}
		else
		{
			Server_Select(ActorToSelect);
		}
	}
	else
	{
		Server_ClearSelection();
	}
}

void AGodPlayerController::BeginPlay()
{
	Super::BeginPlay();
	FInputModeGameAndUI InputMode;
	InputMode.SetHideCursorDuringCapture(false);
	SetInputMode(InputMode);
	bShowMouseCursor = true;
}

void AGodPlayerController::Server_Select_Implementation(AActor* ActorToSelect)
{
	Server_ClearSelection();
	if (ActorToSelect)
	{
		if (auto Selectable = Cast<IISelectable>(ActorToSelect))
		{
			Selectable->Select();
			Selected.Add(ActorToSelect);
			OnRep_Selected();
		}
	}
}

void AGodPlayerController::Server_DeSelect_Implementation(AActor* ActorToDeSelect)
{
	if (ActorToDeSelect)
	{
		if (auto Selectable = Cast<IISelectable>(ActorToDeSelect))
		{
			Selectable->Deselect();
			Selected.Remove(ActorToDeSelect);
			OnRep_Selected();
		}
	}
}

void AGodPlayerController::Server_ClearSelection_Implementation()
{
	for (auto Unit : Selected)
	{
		if (Unit)
		{
			if (auto Selectable = Cast<IISelectable>(Unit))
			{
				Selectable->Deselect();
			}
		}
	}
	Selected.Empty();
	OnRep_Selected();
}

void AGodPlayerController::OnRep_Selected()
{
	OnSelectedUpdated.Broadcast();
}

bool AGodPlayerController::ActorSelected(AActor* ActorToCheck) const
{
	if (ActorToCheck && Selected.Contains(ActorToCheck))
	{
		return true;
	}

	return false;
}
