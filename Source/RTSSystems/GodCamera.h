// All rights reserved Innocent Sign Inn

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GodCamera.generated.h"

UCLASS()
class RTSSYSTEMS_API AGodCamera : public APawn
{
	GENERATED_BODY()

public:
	AGodCamera();

protected:
	virtual void BeginPlay() override;
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

public:
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY()
	class AGodPlayerController* PlayerControllerRef;
	
	UPROPERTY(EditAnywhere, Category = "Camera Components")
	USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere, Category = "Camera Components")
	class USpringArmComponent* CameraBoom;
	
	UPROPERTY(EditAnywhere, Category = "Camera Components")
	class UCameraComponent* MainCamera;

#pragma region Configurations Values
	UPROPERTY(EditAnywhere, Category = "Camera Configuration Values")
	float ZoomInSpeed;
	UPROPERTY(EditAnywhere, Category = "Camera Configuration Values")
	float PanMovementSpeed;
	UPROPERTY(EditAnywhere, Category = "Camera Configuration Values")
	float RotateSpeed;
	UPROPERTY(EditAnywhere, Category = "Camera Configuration Values")
	float MinZoomHeight;
	UPROPERTY(EditAnywhere, Category = "Camera Configuration Values")
	float MaxZoomHeight;
#pragma endregion

#pragma region Input Actions
	UPROPERTY(EditAnywhere, Category="Input Configurations")
	class UInputMappingContext* InputMapping;
	UPROPERTY(EditAnywhere, Category="Input Configurations")
	class UInputAction* PanAction;
	UPROPERTY(EditAnywhere, Category="Input Configurations")
	UInputAction* ZoomAction;
	UPROPERTY(EditAnywhere, Category="Input Configurations")
	UInputAction* RotateAction;
	UPROPERTY(EditAnywhere, Category="Input Configurations")
	UInputAction* LeftMouseDown;
#pragma endregion

#pragma region Input_Callbacks
	void MoveActionCallbackFunc(const struct FInputActionInstance& Instance);

	void RotateCamera(const FInputActionInstance& Instance);

	void ZoomCamera(const FInputActionInstance& Instance);

	void OnLeftMouseDownCallbackFunc(const FInputActionInstance& Instance);
	void OnLeftMouseUpCallbackFunc(const FInputActionInstance& Instance);
#pragma endregion
	FVector TargetLocation;
	FRotator TargetRotation;
	float TargetZoom;
protected:
	UFUNCTION()
	void EdgeScroll();

	void GetTerrainPosition(FVector& TerrainPosition) const;

	UPROPERTY()
	class AGodPlayerController* GodPlayerController;


	UFUNCTION()
	AActor* GetSelectedObject();
};
