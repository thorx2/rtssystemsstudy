// Copyright Epic Games, Inc. All Rights Reserved.

#include "RTSSystems.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, RTSSystems, "RTSSystems" );
