// All rights reserved Innocent Sign Inn

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "ISelectable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UISelectable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class RTSSYSTEMS_API IISelectable
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION()
	virtual void Select() = 0;
	UFUNCTION()
	virtual void Deselect() = 0;
	UFUNCTION()
	virtual void Highlight(bool SSelected) = 0;
};
