// All rights reserved Innocent Sign Inn

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseResourceSource.generated.h"

UCLASS()
class RTSSYSTEMS_API ABaseResourceSource : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABaseResourceSource();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(EditAnywhere)
	float SimpleValue;

	UPROPERTY(EditDefaultsOnly)
	class UStaticMeshComponent* VisualComponent;
};
