// All rights reserved Innocent Sign Inn


#include "BaseResourceSource.h"

// Sets default values
ABaseResourceSource::ABaseResourceSource()
{
	PrimaryActorTick.bCanEverTick = true;
	VisualComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualMesh"));
	RootComponent = VisualComponent;
}

// Called when the game starts or when spawned
void ABaseResourceSource::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABaseResourceSource::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

