// All rights reserved Innocent Sign Inn


#include "BaseUnit.h"

// Sets default values
ABaseUnit::ABaseUnit()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BaseComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Unit"));
	RootComponent = BaseComponent;
	VisualComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualMesh"));
	VisualComponent->SetupAttachment(BaseComponent);
}

// Called when the game starts or when spawned
void ABaseUnit::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABaseUnit::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ABaseUnit::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ABaseUnit::Select()
{
	HasBeenChosen = true;
	Highlight(HasBeenChosen);
}

void ABaseUnit::Deselect()
{
	HasBeenChosen = false;
	Highlight(HasBeenChosen);
}

void ABaseUnit::Highlight(bool SSelected)
{
	TArray<UPrimitiveComponent*> Components;
	GetComponents<UPrimitiveComponent>(Components);
	for (auto VizComp : Components)
	{
		if (auto Prim = Cast<UPrimitiveComponent>(VizComp))
		{
			Prim->SetRenderCustomDepth(SSelected);
		}
	}
}
