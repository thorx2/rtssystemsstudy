// All rights reserved Innocent Sign Inn

#pragma once

#include "CoreMinimal.h"
#include "ISelectable.h"
#include "GameFramework/Pawn.h"
#include "BaseUnit.generated.h"

UCLASS()
class RTSSYSTEMS_API ABaseUnit : public APawn, public IISelectable
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABaseUnit();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	USceneComponent* BaseComponent;

	UPROPERTY(EditAnywhere)
	class UStaticMeshComponent* VisualComponent;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	UFUNCTION()
	virtual void Select() override;
	UFUNCTION()
	virtual void Deselect() override;
	UFUNCTION()
	virtual void Highlight(bool SSelected) override;

	UPROPERTY()
	bool HasBeenChosen;
};
