// All rights reserved Innocent Sign Inn

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GodPlayerController.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSelectedUpdatedDelegate);
/**
 * 
 */
UCLASS()
class RTSSYSTEMS_API AGodPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	AGodPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION()
	void Handle_Selection(AActor* ActorToSelect);
protected:
	virtual void BeginPlay() override;

	UFUNCTION(Server, Reliable)
	void Server_Select(AActor* ActorToSelect);

	UFUNCTION(Server, Reliable)
	void Server_DeSelect(AActor* ActorToDeSelect);

	UFUNCTION(Server, Reliable)
	void Server_ClearSelection();

	UFUNCTION()
	void OnRep_Selected();


	UPROPERTY(ReplicatedUsing= OnRep_Selected);
	TArray<AActor*> Selected;

	UPROPERTY()
	FSelectedUpdatedDelegate OnSelectedUpdated;
private:
	bool ActorSelected(AActor* ActorToCheck) const;
};
